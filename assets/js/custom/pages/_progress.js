//
// Progress
//

// ScrollMagic Controller
var progressCtlr = new ScrollMagic.Controller();

// Elements
var $progress = $('.ww-progress .ww-container');
var $progressSentence = $progress.find('h1, h2, h3');
var $progressSentencePart1 = $progressSentence.filter('.part-1');
var $progressSentencePart2 = $progressSentence.filter('.part-2');

// Timeline
var progressTl = new TimelineMax({ paused:true });
var progressTlPart2 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($progressSentence, {top: '60%', opacity: 0});

// Define Animation
progressTl
  .to($progressSentencePart1, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.off('keyup');
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        progressTlPart2.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 2
progressTlPart2
  .to($progressSentencePart1, .8, {top: '40%', opacity: 0})
  .staggerTo($progressSentencePart2, 1.2, {top: '50%', opacity: 1}, .4)
  .add(function(){
    var target = $reasons.offset().top;
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $document.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $progress[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  progressTl.play(0);
})
.addTo(progressCtlr);
