//
// World
//

// Bring to work loop
var thingsWorldProvides = [
  'challenge',
  'potential',
  'perspective',
  'inspiration',
  'sunsets',
  'snow storms',
  'mountains',
  'oceans',
  'music',
  'beauty'
];
function worldProvides(index){
  if(index < thingsWorldProvides.length){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        $worldSwitch.text(thingsWorldProvides[index]);
        $window.off('keyup');
        worldProvides(index + 1);
      }
    });
  } else {
    target = $beauty.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  }
}

// ScrollMagic Controller
var worldCtlr = new ScrollMagic.Controller();

// Elements
var $world = $('.ww-world .ww-container');
var $worldContent1 = $world.find('.content-1');
var $worldContent2 = $world.find('.content-2');
var $worldPart1 = $world.find('.part-1');
var $worldPart2 = $world.find('.part-2');
var $worldSwitch = $world.find('h1.part-2');

// Timeline
var worldTl = new TimelineMax({ paused:true });
var worldTlPart2 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($worldPart1, {y: 60, opacity: 0});
TweenLite.set($worldPart2, {y: 60, opacity: 0});

// Define Animation
// Part 1
worldTl
  .staggerTo($worldPart1, 1.2, {y: 0, opacity: 1}, .4, '+=.2')
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        worldTlPart2.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 2
worldTlPart2
  .to($worldContent1, .8, {left: '60%', opacity: 0})
  .staggerTo($worldPart2, 1.2, {top: '30%', opacity: 1}, .4, '-=.2')
  .add(function(){
    worldProvides(0);
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $world[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  worldTl.play(0);
})
.addTo(worldCtlr);
