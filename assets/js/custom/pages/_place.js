//
// Place
//

// Bring to work loop
var thingsToBring = [
  'time',
  'energy',
  'motivation',
  'passion',
  'ambition',
  'focus',
  'strength',
  'tenacity',
  'discipline',
  'emotion',
  'humor',
  'empathy',
  'patience',
  'authenticity',
  'openness'
];
function bringToWork(index){
  if(index < thingsToBring.length){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        $placePart2.text(thingsToBring[index]);
        $window.off('keyup');
        bringToWork(index + 1);
      }
    });
  } else {
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        placeTlPart4.play(0);
        $window.off('keyup');
      }
    });
  }
}

// ScrollMagic Controller
var placeCtlr = new ScrollMagic.Controller();

// Elements
var $place = $('.ww-place .ww-container');
var $placePart1 = $place.find('.part-1');
var $placePart2 = $place.find('.part-2');
var $placePart3 = $place.find('.part-3');
var $placePart4 = $place.find('.part-4');

// Timeline
var placeTl = new TimelineMax({ paused:true });
var placeTlPart2 = new TimelineMax({ paused:true });
var placeTlPart3 = new TimelineMax({ paused:true });
var placeTlPart4 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($placePart1, {y: 60, opacity: 0});
TweenLite.set($placePart2, {y: 60, opacity: 0});
TweenLite.set($placePart3, {y: 60, opacity: 0});
TweenLite.set($placePart4, {y: 60, opacity: 0});

// Define Animation
// Part 1
placeTl
  .to($placePart1, 1.2, {y: 0, opacity: 1}, '+=.2')
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        placeTlPart2.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 2
placeTlPart2
  .to($placePart2, 1.2, {y: 0, opacity: 1}, '+=.2')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        placeTlPart3.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 3
placeTlPart3
  .to($placePart1, .8, {y: -60, opacity: 0})
  .to($placePart3, 1.2, {y: 0, opacity: 1}, '-=.2')
  .add(function(){
    bringToWork(0);
  });
// Part 3
placeTlPart4
  .to($placePart3, .8, {y: -60, opacity: 0})
  .to($placePart2, .8, {color: '#ccc'}, '-=.6')
  .to($placePart4, 1.2, {y: 0, opacity: 1}, '-=.2')
  .add(function(){
    var target = $family.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $place[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  placeTl.play(0);
})
.addTo(placeCtlr);
