//
// People
//

// ScrollMagic Controller
var peopleCtlr = new ScrollMagic.Controller();

// Elements
var $people = $('.ww-people .ww-container');
var $peoplePart1 = $people.find('.part-1');
var $peoplePart2 = $people.find('.part-2');
var $peopleSwitch = $people.find('.people-switch');

// Timeline
var peopleTl = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($peoplePart1, {y: 60, opacity: 0});

// Define Animation
// Part 1
peopleTl
  .staggerTo($peoplePart1, 1.2, {y: 0, opacity: 1}, .4, '+=.2')
  .add(function(){
    $window.off('keyup');
    var target = $we.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    })
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $people[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  peopleTl.play(0);
})
.addTo(peopleCtlr);
