//
// Instagram Section
//

// Function to scroll instagram rows
function scrollInstagram(index){
  var target;
  var instaScrollTimer = 1000;
  if (index == 1) {
    index = $instagramRows.length - 1;
    instaScrollTimer = 160000;
  }
  if (index == $instagramRows.length) {
    target = $place.offset().top;
    instaScrollTimer = 1000;
  } else {
    target = $instagramRows.eq(index).offset().top;
  }
  $window.off('keyup');
  $window.on('keyup', function(e){
    if(e.keyCode == 34 || e.keyCode == 39) {
      if($body.scrollTop() < target) {
        $('body, html').animate({scrollTop: target}, instaScrollTimer, 'linear');
      }
      $window.off('keyup');
      scrollInstagram(index + 1);
    }
  });
}

// ScrollMagic Controller
var instragramCtlr = new ScrollMagic.Controller();

// Elements
var $instagram = $('.ww-instagram .ww-container');
var $instagramHeadline = $instagram.find('.headline');
var $instagramRows = $instagram.find('.instagram-row');

// Timeline
var instagramTl = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($instagramHeadline, {y: 60, opacity: 0});

// Define Animation
instagramTl
  // Part 1
  .to($instagramHeadline, 1, {y: 0, opacity: 1, ease: 'ease-in'}, '+=.2')
  .add(function(){
    scrollInstagram(1);
  });

// Loop through each Row
$instagramRows.each(function(i, el){
  // Elements
  var $row = $(this);
  var $rowImage = $row.find('img');
  var $rowCopy = $row.find('h3');

  // Timeline
  var rowTL = new TimelineMax({ paused:true });

  // Set Before State
  TweenLite.set($rowImage, {top: '60%', opacity: 0});
  TweenLite.set($rowCopy, {top: '60%', opacity: 0});

  // Define Animation
  rowTL
    .to($rowImage, .6, {top: '50%', opacity: 1, ease: 'ease-in'})
    .to($rowCopy, .8, {top: '50%', opacity: 1, ease: 'ease-in'}, '-=.2');

  // Create The Scene
  var scene = new ScrollMagic.Scene({
    triggerElement: el,
    triggerHook: .5,
    reverse: false,
  })
  .on('start', function() {
    rowTL.play(0);
  })
  .addTo(instragramCtlr);
});

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $instagram[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  instagramTl.play(0);
})
.addTo(instragramCtlr);
