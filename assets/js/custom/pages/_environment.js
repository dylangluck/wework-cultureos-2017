//
// Environment
//

// ScrollMagic Controller
var environmentCtlr = new ScrollMagic.Controller();

// Elements
var $environment = $('.ww-environment .ww-container');
var $environmentSentence = $environment.find('h1, h2');
var $environmentSentenceLines = $environment.find('.line');
var $environmentSentencePart1 = $environmentSentence.filter('.part-1');
var $environmentSentencePart2 = $environmentSentence.filter('.part-2');
var $environmentSentencePart3 = $environmentSentence.filter('.part-3');
var $environmentSentencePart4 = $environmentSentence.filter('.part-4');
var $environmentSentencePart5 = $environmentSentence.filter('.part-5');
var $environmentSentencePart6 = $environmentSentence.filter('.part-6');
var $environmentSentencePart7 = $environment.find('.part-7');
var $environmentSentencePart7Video = $environmentSentencePart7.find('video')[0];
var $environmentSentencePart8 = $environmentSentence.filter('.part-8');

// Timeline
var environmentTl = new TimelineMax({ paused:true });
var environmentTlPart2 = new TimelineMax({ paused:true });
var environmentTlPart3 = new TimelineMax({ paused:true });
var environmentTlPart4 = new TimelineMax({ paused:true });
var environmentTlPart5 = new TimelineMax({ paused:true });
var environmentTlPart6 = new TimelineMax({ paused:true });
var environmentTlPart7 = new TimelineMax({ paused:true });
var environmentTlPart8 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($environmentSentence, {top: '60%', opacity: 0});
TweenLite.set($environmentSentencePart7, {top: '60%', opacity: 0});
TweenLite.set($environmentSentenceLines, {width: '0%'});

// Define Animation
environmentTl
  .to($environmentSentencePart1, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.off('keyup');
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        environmentTlPart2.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 2
environmentTlPart2
  .to($environmentSentencePart1, .8, {top: '40%', opacity: 0})
  .to($environmentSentencePart2, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        environmentTlPart3.play(0);
        $document.off('keyup');
      }
    });
  })
// Part 3
environmentTlPart3
  .to($environmentSentencePart2, .8, {top: '40%', opacity: 0})
  .to($environmentSentencePart3, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        environmentTlPart4.play(0);
        $document.off('keyup');
      }
    });
  })
// Part 4
environmentTlPart4
  .to($environmentSentencePart3, .8, {top: '40%', opacity: 0})
  .to($environmentSentencePart4, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        environmentTlPart5.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 5
environmentTlPart5
  .to($environmentSentencePart4, .8, {top: '40%', opacity: 0})
  .to($environmentSentencePart5, 1.2, {top: '50%', opacity: 1})
  .to($environmentSentence[0], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        environmentTlPart6.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 6
environmentTlPart6
  .to($environmentSentencePart5, .8, {top: '40%', opacity: 0})
  .to($environmentSentencePart6, 1.2, {top: '50%', opacity: 1})
  .to($environmentSentence[1], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        environmentTlPart7.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 6
environmentTlPart7
  .to($environmentSentencePart6, .8, {top: '40%', opacity: 0})
  .to($environmentSentencePart7, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $environmentSentencePart7Video.play();
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        environmentTlPart8.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 7
environmentTlPart8
  .to($environmentSentencePart7, .8, {top: '40%', opacity: 0})
  .to($environmentSentencePart8, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    var target = $growth.offset().top;
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime*18, 'linear');
        }
        $document.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $environment[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  environmentTl.play(0);
})
.addTo(environmentCtlr);
