//
// Needs
//

// ScrollMagic Controller
var needsCtlr = new ScrollMagic.Controller();

// Elements
var $needs = $('.ww-needs .ww-container');
var $needsH3 = $needs.find('h3');
var $needsH1 = $needs.find('h1');
var $needsPart2 = $needs.find('h1.part-2');
var $needsPart3 = $needs.find('h1.part-3');
var $needsPart4 = $needs.find('h1.part-4');
var $needsPart5 = $needs.find('h1.part-5');
var $needsPart6 = $needs.find('h1.part-6');

// Timeline
var needsTl = new TimelineMax({ paused:true });
var needsTlPart2 = new TimelineMax({ paused:true });
var needsTlPart3 = new TimelineMax({ paused:true });
var needsTlPart4 = new TimelineMax({ paused:true });
var needsTlPart5 = new TimelineMax({ paused:true });
var needsTlPart6 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($needsH3, {top: '45%', opacity: 0});
TweenLite.set($needsH1, {top: '60%', opacity: 0});

// Define Animation
// Part 1
needsTl
  .to($needsH3, 1.2, {top: '35%', opacity: 1}, '+=.2')
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        needsTlPart2.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 2
needsTlPart2
  .to($needsPart2, 1.2, {top: '50%', opacity: 1}, '+=.2')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        needsTlPart3.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 3
needsTlPart3
  .to($needsPart2, .8, {top: '40%', opacity: 0})
  .to($needsPart3, 1.2, {top: '50%', opacity: 1}, '-=.2')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        needsTlPart4.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 4
needsTlPart4
  .to($needsPart3, .8, {top: '40%', opacity: 0})
  .to($needsPart4, 1.2, {top: '50%', opacity: 1}, '-=.2')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        needsTlPart5.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 5
needsTlPart5
  .to($needsH3, .8, {top: '25%', opacity: 0})
  .to($needsPart4, .8, {top: '40%', opacity: 0}, '-=.8')
  .to($needsPart5, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        needsTlPart6.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 6
needsTlPart6
  .to($needsPart5, .8, {top: '40%', opacity: 0})
  .to($needsPart6, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    var target = $iam.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $needs[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  needsTl.play(0);
})
.addTo(needsCtlr);
