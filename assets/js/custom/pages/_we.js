//
// We
//

// ScrollMagic Controller
var weCtlr = new ScrollMagic.Controller();

// Elements
var $we = $('.ww-we .ww-container');
var $weSentence = $we.find('h1, h2');
var $weSentenceLines = $weSentence.find('.line');
var $weSentencePart1 = $weSentence.filter('.part-1');
var $weSentencePart2 = $weSentence.filter('.part-2');
var $weSentencePart3 = $weSentence.filter('.part-3');
var $weSentencePart4 = $weSentence.filter('.part-4');
var $weSentencePart5 = $weSentence.filter('.part-5');
var $weSentencePart6 = $weSentence.filter('.part-6');
var $weSentencePart7 = $weSentence.filter('.part-7');

// Timeline
var weTl = new TimelineMax({ paused:true });
var weTlPart2 = new TimelineMax({ paused:true });
var weTlPart3 = new TimelineMax({ paused:true });
var weTlPart4 = new TimelineMax({ paused:true });
var weTlPart5 = new TimelineMax({ paused:true });
var weTlPart6 = new TimelineMax({ paused:true });
var weTlPart7 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($weSentence, {top: '60%', opacity: 0});
TweenLite.set($weSentenceLines, {width: '0%'});

// Define Animation
weTl
  .to($weSentencePart1, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        weTlPart2.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 2
weTlPart2
  .to($weSentencePart1, .8, {top: '40%', opacity: 0})
  .to($weSentencePart2, 1.2, {top: '50%', opacity: 1})
  .to($weSentenceLines[0], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        weTlPart3.play(0);
        $window.off('keyup');
      }
    });
  })
// Part 3
weTlPart3
  .to($weSentencePart2, .8, {top: '40%', opacity: 0})
  .to($weSentencePart3, 1.2, {top: '50%', opacity: 1})
  .to($weSentenceLines[1], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        weTlPart4.play(0);
        $window.off('keyup');
      }
    });
  })
// Part 4
weTlPart4
  .to($weSentencePart3, .8, {top: '40%', opacity: 0})
  .to($weSentencePart4, 1.2, {top: '50%', opacity: 1})
  .to($weSentenceLines[2], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        weTlPart5.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 5
weTlPart5
  .to($weSentencePart4, .8, {top: '40%', opacity: 0})
  .to($weSentencePart5, 1.2, {top: '50%', opacity: 1})
  .to($weSentenceLines[3], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        weTlPart6.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 6
weTlPart6
  .to($weSentencePart5, .8, {top: '40%', opacity: 0})
  .to($weSentencePart6, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        weTlPart7.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 7
weTlPart7
  .to($weSentencePart6, .8, {top: '40%', opacity: 0})
  .to($weSentencePart7, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    var target = $progress.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $we[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  weTl.play(0);
})
.addTo(weCtlr);
