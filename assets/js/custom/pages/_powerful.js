//
// Powerful
//

// ScrollMagic Controller
var powerfulCtlr = new ScrollMagic.Controller();

// Elements
var $powerful = $('.ww-powerful .ww-container');
var $powerfulH1Start = $powerful.find('h1.start');
var $powerfulH1End = $powerful.find('h1.end');
var $powerfulH1EndLine = $powerfulH1End.find('.line');

// Timeline
var powerfulTl = new TimelineMax({ paused:true });
var powerfulTlPart2 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($powerfulH1Start, {top: '60%', opacity: 0});
TweenLite.set($powerfulH1End, {top: '60%', opacity: 0});
TweenLite.set($powerfulH1EndLine, {width: '0%'});

// Define Animation
// Part 1
powerfulTl
  .to($powerfulH1Start, 1, {top: '50%', opacity: 1, ease: 'ease-in'}, '+=.2')
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        powerfulTlPart2.play(0);
        $window.off('keyup');
      }
    });
  })
// Part 2
powerfulTlPart2
  .to($powerfulH1Start, .6, {top: '40%', opacity: 0, ease: 'ease-in'})
  .to($powerfulH1End, .8, {top: '50%', opacity: 1})
  .to($powerfulH1EndLine, .4, {width: '100%', ease: 'ease-in'}, '-=.1')
  .add(function(){
    var target = $problem.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $powerful[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  powerfulTl.play(0);
})
.addTo(powerfulCtlr);
