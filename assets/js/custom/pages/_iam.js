//
// DWYL Signs Section
//

// Function to loop through I AM sentences
var iamSentences = [
  'am certain',
  'am excited about the journey',
  'love this place',
  'am humble',
  'don\'t hate the idea of waking up in the morning',
  'love the people I\'m surrounded by',
  'am passionate',
  'am excited about the journey',
  'am excited about my life',
  'love my work',
  'am inspired by the people I\'m surrounded by',
  'am excited about my life',
  'am doing the right thing',
  'am having an impact',
  'am changing',
  'am pushing myself',
  'love my life',
  'am not afraid',
  'am at my best',
  'am making the world better',
  'am having an impact',
  'am excited about the journey',
  'love my coworkers',
  'am a part of creating something meaningful',
  'have a strong sense of purpose',
  'am connected to the positive impact I\'m making',
  'am excited about my life',
  'am growing',
  'love my job',
  'love my team',
  'love this company',
  'feel heard',
  'am passionate',
  'love the people I\'m surrounded by',
  'am having a positive impact on the world'
]
var currentIamInstance = 0;
var iamSentenceLoopDelay = 3000;
function iamLoop(){
  if(currentIamInstance < iamSentences.length) {
    $iamSwitchContainer.text(iamSentences[currentIamInstance]);
    currentIamInstance++;
  }
  setTimeout(iamLoop, iamSentenceLoopDelay);
}

// ScrollMagic Controller
var iamCtlr = new ScrollMagic.Controller();

// Elements
var $iam = $('.ww-iam .ww-container');
var $iamH3 = $iam.find('h3');
var $iamSwitch = $iam.find('.iam-switch');
var $iamSwitchContainer = $iamSwitch.find('.switch');


// Timeline
var iamTl = new TimelineMax({ paused:true });
var iamTlPart2 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($iamH3, {top: '25%', opacity: 0});
TweenLite.set($iamSwitch, {top: '40%', opacity: 0});

// Define Animation
// Part 1
iamTl
  .to($iamH3, 1, {top: '15%', opacity: 1, ease: 'ease-in'}, '+=.2')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        iamTlPart2.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 2
iamTlPart2
  .to($iamSwitch, .8, {top: '30%', opacity: 1, ease: 'ease-in'})
  .add(iamLoop, '+=2')
  .add(function(){
    target = $instagram.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $iam[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  iamTl.play(0);
})
.addTo(iamCtlr);
