//
// Problem
//

// ScrollMagic Controller
var problemCtlr = new ScrollMagic.Controller();

// Elements
var $problem = $('.ww-problem .ww-container');
var $contentPart1 = $problem.find('.content.part-1');
var $problemH3Part1 = $problem.find('h3.part-1');
var $problemH3Part1Line = $problemH3Part1.find('.line');
var $problemH2Part1 = $problem.find('h2.part-1');
var $problemH3SpansPart2 = $problem.find('h3.part-2 span');
var $problemH1Part3 = $problem.find('h1.part-3');
var $problemH3SpansPart4 = $problem.find('h3.part-4 span');

// Timeline
var problemTl = new TimelineMax({ paused:true });
var problemTlPart2 = new TimelineMax({ paused:true });
var problemTlPart3 = new TimelineMax({ paused:true });
var problemTlPart4 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($problemH3Part1, {y: 60, opacity: 0});
TweenLite.set($problemH3Part1Line, {width: '0%'});
TweenLite.set($problemH2Part1, {y: 60, opacity: 0});
TweenLite.set($problemH3SpansPart2, {y: 60, opacity: 0});
TweenLite.set($problemH1Part3, {y: 60, opacity: 0});
TweenLite.set($problemH3SpansPart4, {y: 60, opacity: 0});

// Define Animation
// Part 1
problemTl
  .to($problemH3Part1, 1.6, {y: 0, opacity: 1, ease: 'ease-in'}, '+=.2')
  .to($problemH3Part1Line, .6, {width: '100%', ease: 'ease-in-out'})
  .to($problemH2Part1, 1.2, {y: 0, opacity: 1, ease: 'ease-in'}, '-=.2')
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        problemTlPart2.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 2
problemTlPart2
  .to($contentPart1, .8, {left: '60%', opacity: 0, ease: 'ease-in'})
  .staggerTo($problemH3SpansPart2, .8, {y: 0, opacity: 1, ease: 'ease-in'}, .4, '-=.1')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        problemTlPart3.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 3
problemTlPart3
  .to($problemH3SpansPart2, .4, {y: -60, opacity: 0, ease: 'ease-in'})
  .to($problemH1Part3, 1, {y: 0, opacity: 1, ease: 'ease-in'}, '+=.2')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        problemTlPart4.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 4
problemTlPart4
  .to($problemH3SpansPart4, .8, {y: 0, opacity: 1, ease: 'ease-in'})
  .add(function(){
    var target = $needs.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  });



// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $problem[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  problemTl.play(0);
})
.addTo(problemCtlr);
