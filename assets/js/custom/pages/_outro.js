//
// Outro
//

// outro loop
var weHave = [
  'Structure',
  'Organization',
  'Goals',
  'Budgets',
  'Performance Agility',
  'Modular Functions',
  'Regions',
  'Talent acquisition',
  'Training',
  'Feedback',
  'Communication',
  'Growth & Development',
  'Recognition & Reward'
];
function weHaveLoop(index){
  if(index < weHave.length){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        $outroSwitch.text(weHave[index]);
        $document.off('keyup');
        weHaveLoop(index + 1);
      }
    });
  } else {
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart5.play(0);
        $document.off('keyup');
      }
    });
  }
}

// ScrollMagic Controller
var outroCtlr = new ScrollMagic.Controller();

// Elements
var $outro = $('.ww-outro .ww-container');
var $outroSentence = $outro.find('h1, h2');
var $outroSwitch = $outro.find('.outro-switch');
var $outroSentencePart1 = $outroSentence.filter('.part-1');
var $outroSentencePart2 = $outroSentence.filter('.part-2');
var $outroSentencePart3 = $outroSentence.filter('.part-3');
var $outroSentencePart4 = $outroSentence.filter('.part-4');
var $outroSentencePart5 = $outroSentence.filter('.part-5');
var $outroSentencePart6 = $outroSentence.filter('.part-6');
var $outroSentencePart7 = $outroSentence.filter('.part-7');
var $outroSentencePart8 = $outroSentence.filter('.part-8');
var $outroSentencePart9 = $outroSentence.filter('.part-9');
var $outroSentencePart10 = $outroSentence.filter('.part-10');
var $outroSentencePart11 = $outroSentence.filter('.part-11');
var $outroSentencePart12 = $outroSentence.filter('.part-12');
var $outroSentencePart13 = $outroSentence.filter('.part-13');
var $outroSentencePart14 = $outroSentence.filter('.part-14');
var $outroSentencePart15 = $outroSentence.filter('.part-15');

// Timeline
var outroTl = new TimelineMax({ paused:true });
var outroTlPart2 = new TimelineMax({ paused:true });
var outroTlPart3 = new TimelineMax({ paused:true });
var outroTlPart4 = new TimelineMax({ paused:true });
var outroTlPart5 = new TimelineMax({ paused:true });
var outroTlPart6 = new TimelineMax({ paused:true });
var outroTlPart7 = new TimelineMax({ paused:true });
var outroTlPart8 = new TimelineMax({ paused:true });
var outroTlPart9 = new TimelineMax({ paused:true });
var outroTlPart10 = new TimelineMax({ paused:true });
var outroTlPart11 = new TimelineMax({ paused:true });
var outroTlPart12 = new TimelineMax({ paused:true });
var outroTlPart13 = new TimelineMax({ paused:true });
var outroTlPart14 = new TimelineMax({ paused:true });
var outroTlPart15 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($outroSentence, {top: '60%', opacity: 0});

// Define Animation
outroTl
  .to($outroSentencePart1, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart2.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 2
outroTlPart2
  .to($outroSentencePart1, .8, {top: '40%', opacity: 0})
  .to($outroSentencePart2, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart3.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 3
outroTlPart3
  .to($outroSentencePart2, .8, {top: '40%', opacity: 0})
  .to($outroSentencePart3, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart4.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 4
outroTlPart4
.to($outroSentencePart3, .8, {top: '40%', opacity: 0})
.to($outroSentencePart4, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    weHaveLoop(0);
  });
// Part 5
outroTlPart5
.to($outroSentencePart4, .8, {top: '40%', opacity: 0})
.to($outroSentencePart5, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart6.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 6
outroTlPart6
  .to($outroSentencePart5, .8, {top: '40%', opacity: 0})
  .to($outroSentencePart6, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart7.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 7
outroTlPart7
  .to($outroSentencePart6, .8, {top: '40%', opacity: 0})
  .to($outroSentencePart7, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart8.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 8
outroTlPart8
  .to($outroSentencePart7, .8, {top: '40%', opacity: 0})
  .to($outroSentencePart8, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart9.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 9
outroTlPart9
  .to($outroSentencePart8, .8, {top: '40%', opacity: 0})
  .to($outroSentencePart9, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart10.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 10
outroTlPart10
  .to($outroSentencePart9, .8, {top: '40%', opacity: 0})
  .to($outroSentencePart10, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart11.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 11
outroTlPart11
  .to($outroSentencePart10, .8, {top: '40%', opacity: 0})
  .to($outroSentencePart11, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart12.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 12
outroTlPart12
  .to($outroSentencePart11, .8, {top: '40%', opacity: 0})
  .to($outroSentencePart12, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart13.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 13
outroTlPart13
  .to($outroSentencePart12, .8, {top: '40%', opacity: 0})
  .to($outroSentencePart13, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart14.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 14
outroTlPart14
  .to($outroSentencePart13, .8, {top: '40%', opacity: 0})
  .to($outroSentencePart14, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        outroTlPart15.play(0);
        $document.off('keyup');
      }
    });
  });
// Part 15
outroTlPart15
  .to($outroSentencePart14, .8, {top: '40%', opacity: 0})
  .to($outroSentencePart15, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    var target = $final.offset().top;
    $document.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $document.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $outro[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  outroTl.play(0);
})
.addTo(outroCtlr);
