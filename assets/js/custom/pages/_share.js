//
// Share
//

// ScrollMagic Controller
var shareCtlr = new ScrollMagic.Controller();

// Elements
var $share = $('.ww-share .ww-container');
var $shareSentence = $share.find('h1, h2');
var $shareSentencePart1 = $shareSentence.filter('.part-1');
var $shareSentencePart2 = $shareSentence.filter('.part-2');
var $shareSentencePart3 = $shareSentence.filter('.part-3');
var $shareSentencePart4 = $shareSentence.filter('.part-4');
var $shareSentencePart5 = $shareSentence.filter('.part-5');

// Timeline
var shareTl = new TimelineMax({ paused:true });
var shareTlPart2 = new TimelineMax({ paused:true });
var shareTlPart3 = new TimelineMax({ paused:true });
var shareTlPart4 = new TimelineMax({ paused:true });
var shareTlPart5 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($shareSentence, {top: '60%', opacity: 0});

// Define Animation
shareTl
  .to($shareSentencePart1, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        shareTlPart2.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 2
shareTlPart2
  .to($shareSentencePart1, .8, {top: '40%', opacity: 0})
  .to($shareSentencePart2, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        shareTlPart3.play(0);
        $window.off('keyup');
      }
    });
  })
// Part 3
shareTlPart3
  .to($shareSentencePart2, .8, {top: '40%', opacity: 0})
  .to($shareSentencePart3, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        shareTlPart4.play(0);
        $window.off('keyup');
      }
    });
  })
// Part 4
shareTlPart4
  .to($shareSentencePart3, .8, {top: '40%', opacity: 0})
  .to($shareSentencePart4, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        shareTlPart5.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 5
shareTlPart5
  .to($shareSentencePart4, .8, {top: '40%', opacity: 0})
  .to($shareSentencePart5, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    var target = $environment.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $share[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  shareTl.play(0);
})
.addTo(shareCtlr);
