//
// You
//

// ScrollMagic Controller
var youCtlr = new ScrollMagic.Controller();

// Elements
var $you = $('.ww-you .ww-container');
var $youSentence = $you.find('h1, h2');
var $youSentenceLines = $youSentence.find('.line');
var $youSentencePart1 = $youSentence.filter('.part-1');
var $youSentencePart2 = $youSentence.filter('.part-2');
var $youSentencePart3 = $youSentence.filter('.part-3');
var $youSentencePart4 = $youSentence.filter('.part-4');
var $youSentencePart5 = $youSentence.filter('.part-5');
var $youSentencePart6 = $youSentence.filter('.part-6');
var $youSentencePart7 = $youSentence.filter('.part-7');

// Timeline
var youTl = new TimelineMax({ paused:true });
var youTlPart2 = new TimelineMax({ paused:true });
var youTlPart3 = new TimelineMax({ paused:true });
var youTlPart4 = new TimelineMax({ paused:true });
var youTlPart5 = new TimelineMax({ paused:true });
var youTlPart6 = new TimelineMax({ paused:true });
var youTlPart7 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($youSentence, {top: '60%', opacity: 0});
TweenLite.set($youSentenceLines, {width: '0%'});

// Define Animation
youTl
  .to($youSentencePart1, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        youTlPart2.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 2
youTlPart2
  .to($youSentencePart1, .8, {top: '40%', opacity: 0})
  .to($youSentencePart2, 1.2, {top: '50%', opacity: 1})
  .to($youSentenceLines[0], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        youTlPart3.play(0);
        $window.off('keyup');
      }
    });
  })
// Part 3
youTlPart3
  .to($youSentencePart2, .8, {top: '40%', opacity: 0})
  .to($youSentencePart3, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        youTlPart4.play(0);
        $window.off('keyup');
      }
    });
  })
// Part 4
youTlPart4
  .to($youSentencePart3, .8, {top: '40%', opacity: 0})
  .to($youSentencePart4, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        youTlPart5.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 5
youTlPart5
  .to($youSentencePart4, .8, {top: '40%', opacity: 0})
  .to($youSentencePart5, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        youTlPart6.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 6
youTlPart6
  .to($youSentencePart5, .8, {top: '40%', opacity: 0})
  .to($youSentencePart6, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        youTlPart7.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 7
youTlPart7
  .to($youSentencePart6, .8, {top: '40%', opacity: 0})
  .to($youSentencePart7, 1.2, {top: '50%', opacity: 1})
  .to($youSentenceLines[1], .4, {width: '100%'}, '-=.4')
  .add(function(){
    var target = $share.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $you[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  youTl.play(0);
})
.addTo(youCtlr);
