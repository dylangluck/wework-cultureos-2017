//
// Family
//

// Bring to work loop
var thingsFamilyProvides = [
  'connection',
  'reflection',
  'relaxation',
  'grounding',
  'validation',
  'encouragement',
  'empathy'
];
function familyProvides(index){
  if(index <= thingsFamilyProvides.length){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        $familyPart3.text(thingsFamilyProvides[index]);
        $window.off('keyup');
        familyProvides(index + 1);
      }
    });
  } else {
    familyTlPart4.play(0);
  }
}

// ScrollMagic Controller
var familyCtlr = new ScrollMagic.Controller();

// Elements
var $family = $('.ww-family .ww-container');
var $familyPart1 = $family.find('.part-1');
var $familyPart2 = $family.find('.part-2');
var $familyPart3 = $family.find('.part-3');
var $familyPart4 = $family.find('.part-4');
var $familyPart5 = $family.find('.part-5');
var $familyPart6 = $family.find('.part-6');

// Timeline
var familyTl = new TimelineMax({ paused:true });
var familyTlPart2 = new TimelineMax({ paused:true });
var familyTlPart4 = new TimelineMax({ paused:true });
var familyTlPart5 = new TimelineMax({ paused:true });
var familyTlPart6 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($familyPart1, {top: '60%', opacity: 0});
TweenLite.set($familyPart2, {top: '40%', opacity: 0});
TweenLite.set($familyPart3, {top: '60%', opacity: 0});
TweenLite.set($familyPart4, {top: '60%', opacity: 0});
TweenLite.set($familyPart5, {top: '60%', opacity: 0});
TweenLite.set($familyPart6, {top: '60%', opacity: 0});

// Define Animation
// Part 1
familyTl
  .to($familyPart1, 1.2, {top: '50%', opacity: 1}, '+=.2')
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        familyTlPart2.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 2
familyTlPart2
  .to($familyPart1, .8, {top: '40%', opacity: 0})
  .to($familyPart2, 1.2, {top: '30%', opacity: 1}, '-=.2')
  .to($familyPart3, 1.2, {top: '50%', opacity: 1}, '-=.2')
  .add(function(){
    familyProvides(0);
  });
// Part 4
familyTlPart4
  .to($familyPart2, .8, {top: '20%', opacity: 0})
  .to($familyPart3, .8, {top: '40%', opacity: 0}, '-=.8')
  .to($familyPart4, 1.2, {top: '50%', opacity: 1}, '-=.2')
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        familyTlPart5.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 5
familyTlPart5
  .to($familyPart4, .8, {top: '40%', opacity: 0})
  .to($familyPart5, 1.2, {top: '50%', opacity: 1}, '-=.2')
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        familyTlPart6.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 6
familyTlPart6
  .to($familyPart5, .8, {top: '40%', opacity: 0})
  .to($familyPart6, 1.2, {top: '50%', opacity: 1}, '-=.2')
  .add(function(){
    target = $world.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $family[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  familyTl.play(0);
})
.addTo(familyCtlr);
