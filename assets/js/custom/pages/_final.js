//
// Final Section
//

// ScrollMagic Controller
var finalCtlr = new ScrollMagic.Controller();

// Elements
var $final = $('.ww-final .ww-container');
var $finalDWYL = $final.find('.ww-dwyl');

// Timeline
var finalTl = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($finalDWYL, {top: '60%', opacity: 0});

// Define Animation
finalTl
  .to($finalDWYL, 1.6, {opacity: 1, ease: 'ease-in'}, '+=.2')
  .to($finalDWYL, .6, {top: '50%', ease: 'ease-in'}, '-=1.6')
  .add(function(){
    $window.off('keyup');
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $final[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  finalTl.play(0);
})
.addTo(finalCtlr);
