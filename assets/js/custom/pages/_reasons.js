//
// Reasons
//

// People loop
var reasonsList = [
  'Billing issues',
  'Maintenance tickets',
  'Constant change',
  'Confusing roles',
  'Disorganization',
  'Lack of clarity',
  'Lack of communication',
  'Overcrowding',
  'Not enough conference rooms',
  'Siloed Departments',
  'Long hours',
  'Demanding deadlines',
  'Growing too fast',
  'Lack of professional development',
  'Bad management',
  'Not enough training'
];
var reasonsDelay = 2000;
function reasonsLoop(index){
  if(index < reasonsList.length){
    $window.off('keyup');
    $reasonsSwitch.text(reasonsList[index]);
    if(index == reasonsList.length - 1) {
      index = 0;
    }
    setTimeout(function(){
      reasonsLoop(index + 1);
    }, reasonsDelay);
  }
  var target = $outro.offset().top;
  $window.on('keyup', function(e){
    if(e.keyCode == 34 || e.keyCode == 39) {
      if($body.scrollTop() < target) {
        $('body, html').animate({scrollTop: target}, sectionScrollTime);
      }
      $window.off('keyup');
    }
  })
}

// ScrollMagic Controller
var reasonsCtlr = new ScrollMagic.Controller();

// Elements
var $reasons = $('.ww-reasons .ww-container');
var $reasonsPart1 = $reasons.find('.part-1');
var $reasonsPart2 = $reasons.find('.part-2');
var $reasonsSwitch = $reasons.find('.reasons-switch');

// Timeline
var reasonsTl = new TimelineMax({ paused:true });
var reasonsTlPart2 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($reasonsPart1, {y: 60, opacity: 0});
TweenLite.set($reasonsPart2, {y: 60, opacity: 0});

// Define Animation
// Part 1
reasonsTl
  .to($reasonsPart1, 1.2, {y: 0, opacity: 1}, .4, '+=.2')
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        reasonsTlPart2.play();
        $window.off('keyup');
      }
    });
  });
// Part 2
reasonsTlPart2
  .to($reasonsPart2, 1.2, {y: 0, opacity: 1}, '-=.2')
  .add(function(){
    reasonsLoop(0);
  }, '+=2');

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $reasons[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  reasonsTl.play(0);
})
.addTo(reasonsCtlr);
