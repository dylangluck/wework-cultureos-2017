//
// Countries
//

// ScrollMagic Controller
var countriesCtlr = new ScrollMagic.Controller();

// Elements
var $countries = $('.ww-countries .ww-container');
var $countryH3 = $countries.find('h3');

// Set Before State
TweenLite.set($countryH3, {y: 60, opacity: 0});

// Loop through each country
$countryH3.each(function(i, el){

  // Timeline
  var countriesTl = new TimelineMax({ paused:true });

  // Elements
  var $country = $(el);

  // Define Animation
  countriesTl
    .to($country, .7, {y: 0, opacity: 1, ease: 'ease-in'});


  // Create The Scene
  var scene = new ScrollMagic.Scene({
    triggerElement: el,
    triggerHook: .8,
    reverse: false,
  })
  .on('enter', function() {
    countriesTl.play(0);
  })
  .addTo(countriesCtlr);

});
