//
// Beauty
//

// ScrollMagic Controller
var beautyCtlr = new ScrollMagic.Controller();

// Elements
var $beauty = $('.ww-beauty .ww-container');
var $beautySentence = $beauty.find('h1, h2');
var $beautySentenceLines = $beautySentence.find('.line');
var $beautySentencePart1 = $beautySentence.filter('.part-1');
var $beautySentencePart2 = $beautySentence.filter('.part-2');
var $beautySentencePart3 = $beautySentence.filter('.part-3');
var $beautySentencePart4 = $beautySentence.filter('.part-4');
var $beautySentencePart5 = $beautySentence.filter('.part-5');
var $beautySentencePart6 = $beautySentence.filter('.part-6');
var $beautySentencePart7 = $beautySentence.filter('.part-7');

// Timeline
var beautyTl = new TimelineMax({ paused:true });
var beautyTlPart2 = new TimelineMax({ paused:true });
var beautyTlPart3 = new TimelineMax({ paused:true });
var beautyTlPart4 = new TimelineMax({ paused:true });
var beautyTlPart5 = new TimelineMax({ paused:true });
var beautyTlPart6 = new TimelineMax({ paused:true });
var beautyTlPart7 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($beautySentence, {top: '60%', opacity: 0});
TweenLite.set($beautySentenceLines, {width: '0%'});

// Define Animation
beautyTl
  .to($beautySentencePart1, 1.2, {top: '50%', opacity: 1})
  .to($beautySentenceLines[0], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        beautyTlPart2.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 2
beautyTlPart2
  .to($beautySentencePart1, .8, {top: '40%', opacity: 0})
  .to($beautySentencePart2, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        beautyTlPart3.play(0);
        $window.off('keyup');
      }
    });
  })
// Part 3
beautyTlPart3
  .to($beautySentencePart2, .8, {top: '40%', opacity: 0})
  .to($beautySentencePart3, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        beautyTlPart4.play(0);
        $window.off('keyup');
      }
    });
  })
// Part 4
beautyTlPart4
  .to($beautySentencePart3, .8, {top: '40%', opacity: 0})
  .to($beautySentencePart4, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        beautyTlPart5.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 5
beautyTlPart5
  .to($beautySentencePart4, .8, {top: '40%', opacity: 0})
  .to($beautySentencePart5, 1.2, {top: '50%', opacity: 1})
  .to($beautySentenceLines[1], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        beautyTlPart6.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 6
beautyTlPart6
  .to($beautySentencePart5, .8, {top: '40%', opacity: 0})
  .to($beautySentencePart6, 1.2, {top: '50%', opacity: 1})
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        beautyTlPart7.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 7
beautyTlPart7
  .to($beautySentencePart6, .8, {top: '40%', opacity: 0})
  .to($beautySentencePart7, 1.2, {top: '50%', opacity: 1})
  .to($beautySentenceLines[2], .4, {width: '100%'}, '-=.4')
  .add(function(){
    var target = $you.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $beauty[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  beautyTl.play(0);
})
.addTo(beautyCtlr);
