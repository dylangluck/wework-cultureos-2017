//
// Growth
//

// opportunities loop
var opportunities = [
  'develop',
  'change',
  'evolve',
  'find yourself',
  'be yourself',
  'see yourself',
  'understand how you fit in',
  'understand how you contribute',
  'understand how you make an impact',
  'make an impact',
  'be recognized',
  'be celebrated',
  'be compensated',
  'be rewarded',
  'be connected',
  'feel connected'
];
function opportunitiesLoop(index){
  if(index < opportunities.length){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        $growthSwitch.text(opportunities[index]);
        $window.off('keyup');
        opportunitiesLoop(index + 1);
      }
    });
  } else {
    console.log('setup');
    $window.off('keyup');
    target = $people.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  }
}

// Globall Oportunities
function globalOportunities() {
  $window.off('keyup');
  $window.on('keyup', function(e){
    if(e.keyCode == 34 || e.keyCode == 39) {
      $growthGlobalSwitch.text('opportunity');
      $window.off('keyup');
      $window.on('keyup', function(e){
        if(e.keyCode == 34 || e.keyCode == 39) {
          growthTlPart2.play(0);
          $window.off('keyup');
        }
      });
    }
  });
}

// ScrollMagic Controller
var growthCtlr = new ScrollMagic.Controller();

// Elements
var $growth = $('.ww-growth .ww-container');
var $growthContent1 = $growth.find('.content-1');
var $growthContent2 = $growth.find('.content-2');
var $growthPart1 = $growth.find('.part-1');
var $growthPart2 = $growth.find('.part-2');
var $growthGlobalSwitch = $growth.find('h1.part-1');
var $growthSwitch = $growth.find('.growth-switch');

// Timeline
var growthTl = new TimelineMax({ paused:true });
var growthTlPart2 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($growthPart1, {y: 60, opacity: 0});
TweenLite.set($growthPart2, {y: 60, opacity: 0});

// Define Animation
// Part 1
growthTl
  .staggerTo($growthPart1, 1.2, {y: 0, opacity: 1}, .4, '+=.2')
  .add(globalOportunities);
// Part 3
growthTlPart2
  .to($growthContent1, .8, {left: '60%', opacity: 0})
  .staggerTo($growthPart2, 1.2, {y: 0, opacity: 1}, .4, '-=.2')
  .add(function(){
    opportunitiesLoop(0);
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $growth[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  growthTl.play(0);
})
.addTo(growthCtlr);
