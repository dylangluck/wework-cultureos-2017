//
// DWYL Signs Section
//

// Function to loop through DWYL signs
var currentImage = 2;
var maxImages = 20;
var imageTimeout = 1200;
function dwylImageLoop(){
  if(currentImage <= maxImages) {
    $dwylImage.attr('src', '/assets/images/dwyl-images/slide-' + currentImage + '.jpg');
    if(currentImage == maxImages) {
      currentImage = 1;
    } else {
      currentImage++;
    }
  }
  setTimeout(dwylImageLoop, imageTimeout);
}

// ScrollMagic Controller
var dwylCtlr = new ScrollMagic.Controller();

// Elements
var $dwyl = $('.ww-dwyl .ww-container');
var $dwylH1Start = $dwyl.find('h1.start');
var $dwylSupportStart = $dwyl.find('h3.start span');
var $dwylH1End = $dwyl.find('h1.end');
var $dwylSupportEnd = $dwyl.find('h3.end span');
var $dwylImage = $dwyl.find('.dwyl-image-swap');

// Timeline
var dwylTl = new TimelineMax({ paused:true });
var dwylTlPart2 = new TimelineMax({ paused:true });
var dwylTlPart3 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($dwylH1Start, {y: 60, opacity: 0});
TweenLite.set($dwylSupportStart, {y: 60, opacity: 0});
TweenLite.set($dwylH1End, {y: 60, opacity: 0});
TweenLite.set($dwylSupportEnd, {y: 60, opacity: 0});
TweenLite.set($dwylImage, {top: '60%', opacity: 0});

// Define Animation
// Part 1
dwylTl
  .to($dwylH1Start, 1.2, {y: 0, opacity: 1, ease: 'ease-in'}, '+=.5')
  .to($dwylImage, .8, {top: '50%', opacity: 1, ease: 'ease-in'}, '-=.6')
  .staggerTo($dwylSupportStart, .6, {y: 0, opacity: 1, ease: 'ease-in'}, .2)
  .add(dwylImageLoop, '+=.1')
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      // Next
      if(e.keyCode == 34 || e.keyCode == 39) {
        dwylTlPart2.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 2
dwylTlPart2
  .to($dwylH1Start, .8, {y: -60, opacity: 0, ease: 'ease-in'})
  .staggerTo($dwylSupportStart, .6, {y: -60, opacity: 0, ease: 'ease-in'}, .2, '-=.4')
  .to($dwylImage, 1, {left: '60%'}, '-=.6')
  .staggerTo($dwylSupportEnd, 1, {y: 0, opacity: 1, ease: 'ease-in'}, .2)
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        dwylTlPart3.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 3
dwylTlPart3
  .to($dwylH1End, 1.6, {y: 0, opacity: 1, ease: 'ease-in'})
  .add(function(){
    var target = $powerful.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  });

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $dwyl[0],
  triggerHook: .3,
  reverse: false,
})
.on('enter', function() {
  dwylTl.play(0);
})
.addTo(dwylCtlr);
