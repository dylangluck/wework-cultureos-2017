//
// Songs
/*

// ScrollMagic Controller
var songsCtlr = new ScrollMagic.Controller();

// Elements
var $songs = $('.ww-songs .ww-container');
var $songsLyrics = $songs.find('h1');
var $songsLyricsLines = $songsLyrics.find('.line');
var $songsLyricsPart1 = $songsLyrics.filter('.part-1');
var $songsLyricsPart2 = $songsLyrics.filter('.part-2');
var $songsLyricsPart3 = $songsLyrics.filter('.part-3');
var $songsLyricsPart4 = $songsLyrics.filter('.part-4');
var $songsLyricsPart5 = $songsLyrics.filter('.part-5');

// Timeline
var songsTl = new TimelineMax({ paused:true });
var songsTlPart2 = new TimelineMax({ paused:true });
var songsTlPart3 = new TimelineMax({ paused:true });
var songsTlPart4 = new TimelineMax({ paused:true });
var songsTlPart5 = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($songsLyrics, {top: '60%', opacity: 0});
TweenLite.set($songsLyricsLines, {width: '0%'});

// Define Animation
songsTl
  .to($songsLyricsPart1, 1.2, {top: '50%', opacity: 1})
  .to($songsLyricsLines[0], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        songsTlPart2.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 2
songsTlPart2
  .to($songsLyricsPart1, .8, {top: '40%', opacity: 0})
  .to($songsLyricsPart2, 1.2, {top: '50%', opacity: 1})
  .to($songsLyricsLines[1], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        songsTlPart3.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 3
songsTlPart3
  .to($songsLyricsPart2, .8, {top: '40%', opacity: 0})
  .to($songsLyricsPart3, 1.2, {top: '50%', opacity: 1})
  .to($songsLyricsLines[2], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        songsTlPart4.play(0);
        $window.off('keyup');
      }
    });
  });
// Part 4
songsTlPart4
  .to($songsLyricsPart3, .8, {top: '40%', opacity: 0})
  .to($songsLyricsPart4, 1.2, {top: '50%', opacity: 1})
  .to($songsLyricsLines[3], .4, {width: '100%'}, '-=.4')
  .add(function(){
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        songsTlPart5.play(0);
        $window.off('keyup');
      }
    });
  })
// Part 5
songsTlPart5
  .to($songsLyricsPart4, .8, {top: '40%', opacity: 0})
  .to($songsLyricsPart5, 1.2, {top: '50%', opacity: 1})
  .to($songsLyricsLines[4], .4, {width: '100%'}, '-=.4')
  .add(function(){
    var target = $needs.offset().top;
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  })

// Create The Scene
var scene = new ScrollMagic.Scene({
  triggerElement: $songs[0],
  triggerHook: .3,
  reverse: false,
})
.on('start', function() {
  songsTl.play(0);
})
.addTo(songsCtlr); */
