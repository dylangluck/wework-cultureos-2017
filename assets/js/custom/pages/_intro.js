//
// Intro Section
//

// Docready show content
$document.ready(function(){
  $('html, body').animate({scrollTop: 0}, 0);
  $('body').animate({opacity: 1}, 500);
});

// ScrollMagic Controller
var introCtlr = new ScrollMagic.Controller();

// Elements
var $intro = $('.ww-intro .ww-container');
var $introLogo = $intro.find('.ww-logo');
var $introDWYL = $intro.find('.ww-dwyl');

// Timeline
var introTl = new TimelineMax({ paused:true });

// Set Before State
TweenLite.set($introLogo, {top: '60%', opacity: 0});
TweenLite.set($introDWYL, {scale: .8, opacity: 0});

// Define Animation
introTl
  .to($introLogo, 1, {top: '50%', opacity: 1, ease: 'ease-in'}, '+=.5')
  .to($introLogo, 1, {width: 150, top: '6%', ease: 'ease-in'}, '+=1')
  .to($introDWYL, 1.2, {opacity: 1, ease: 'ease-in'}, '-=.4')
  .to($introDWYL, .4, {scale: 1, ease: 'ease-in'}, '-=1.2')
  .add(function(){
    var target = $dwyl.offset().top;
    $window.off('keyup');
    $window.on('keyup', function(e){
      if(e.keyCode == 34 || e.keyCode == 39) {
        if($body.scrollTop() < target) {
          $('body, html').animate({scrollTop: target}, sectionScrollTime);
        }
        $window.off('keyup');
      }
    });
  });

// Play first scene on spacebar
$window.on('keyup', function(e){
  if(e.keyCode == 34 || e.keyCode == 39) {
    introTl.play(0);
    $window.off('keyup');
  }
});
