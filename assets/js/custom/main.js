//
// Global Variables
//

// Elements
var $window = $(window);
var $document = $(document);
var $body = $('body');

// Timing Variables
var sectionDelay = '+=6';
var sectionScrollTime = 1000;

//
// Device Info
//

// Is Touch Device ?
var isMobile = Modernizr.touch;

//
// Global Back a slide
//
/*
var sectionOffsets = [];
$('.ww-container').each(function(){
  sectionOffsets.push($(this).offset().top);
});
$window.on('keyup', function(e){
  var scrollTop = $window.scrollTop();
  var target;
  var targetIndex;
  if(e.keyCode == 116 || e.keyCode == 27) {
    $.each(sectionOffsets, function(i, value){
      if(scrollTop > value) {
        target = value;
        targetIndex = i;
      }
    });
    $('body, html').animate({scrollTop: target}, sectionScrollTime);
    $window.on('keyup', function(e){
      if(e.keyCode == 190) {
        target = sectionOffsets[targetIndex+1];
      }
      $('body, html').animate({scrollTop: target}, sectionScrollTime);
    });
  }
}); */
