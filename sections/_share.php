<div class="ww-wrap ww-share">
  <div class="ww-container">
    <h2 class="part-1">A place where we share a vision</h2>
    <h1 class="part-2">Where we believe in a mission</h1>
    <h1 class="part-3">Where we share values</h1>
    <h1 class="part-4">Intention</h1>
    <h1 class="part-5">Meaning</h1>
  </div>
</div>
