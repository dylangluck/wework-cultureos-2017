<div class="ww-wrap ww-we">
  <div class="ww-container">
    <h2 class="part-1">We all have to feel connected to each other</h2>
    <h2 class="part-2">We all have to <span class="yellow">trust<i class="line"></i></span> each other</h2>
    <h2 class="part-3">We all have to <span class="yellow">respect<i class="line"></i></span> each other</h2>
    <h2 class="part-4">We all have to <span class="yellow">believe in<i class="line"></i></span> each other</h2>
    <h2 class="part-5">We all have to <span class="yellow">work with<i class="line"></i></span> each other</h2>
    <h1 class="part-6">And that’s not always easy</h1>
    <h1 class="part-7">Because we’re a work in progress</h1>
  </div>
</div>
