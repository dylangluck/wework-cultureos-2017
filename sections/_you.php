<div class="ww-wrap ww-you">
  <div class="ww-container">
    <h1 class="part-1">You are getting there</h1>
    <h1 class="part-2"><span class="yellow">We<i class="line"></i></span> are getting there</h1>
    <h1 class="part-3">We are all here together</h1>
    <h1 class="part-4">We are WeWork</h1>
    <h1 class="part-5">The right place for you</h1>
    <h1 class="part-6">The right place for us</h1>
    <h2 class="part-7">If WeWork is going to be a<br /> place of love for <span class="yellow">all of us<i class="line"></i></span></h2>
  </div>
</div>
