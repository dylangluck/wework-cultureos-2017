<div class="ww-wrap ww-environment">
  <div class="ww-container">
    <h1 class="part-1">A great work environment</h1>
    <h1 class="part-2">An inspiring design</h1>
    <h2 class="part-3">It has to be filled<br /> with awesome people</h2>
    <h2 class="part-4">You have to be surrounded<br /> by passionate people</h2>
    <h1 class="part-5">You <span class="yellow">are<i class="line"></i></span> surrounded<br /> by passionate people</h1>
    <h1 class="part-6"><span class="yellow">You are<i class="line"></i></span> amazing people</h1>
    <div class="part-7">
      <video id="ww-culture-video">
        <source src="/assets/videos/culture.mp4" type="video/mp4" />
      </video>
    </div>
    <h2 class="part-8">People from around the world</h2>
  </div>
</div>
