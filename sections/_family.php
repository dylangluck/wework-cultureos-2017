<div class="ww-wrap ww-family">
  <div class="ww-container">
    <h1 class="part-1">Your friends, your family</h1>
    <h2 class="part-2">They provide</h2>
    <h1 class="part-3">Support</h1>
    <h1 class="part-4">They help you know yourself</h1>
    <h1 class="part-5">They give the work meaning</h1>
    <h1 class="part-6">They give life meaning</h1>
  </div>
</div>
