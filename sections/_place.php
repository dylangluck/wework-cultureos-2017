<div class="ww-wrap ww-place">
  <div class="ww-container">
    <h3 class="part-1"><span>When you’re in a place of love</span> <span>you can be yourself</span></h3>
    <h2 class="part-3">You have to bring</h2>
    <h1 class="part-2">Your best self</h1>
    <h1 class="part-4">You have to be open</h1>
  </div>
</div>
