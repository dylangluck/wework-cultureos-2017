<div class="ww-wrap ww-beauty">
  <div class="ww-container">
    <h2 class="part-1">Through all of the complexity, <br />you have to find <span class="yellow">beauty<i class="line"></i></span></h2>
    <h1 class="part-2">simplicity</h1>
    <h1 class="part-3">clarity</h1>
    <h1 class="part-4">certainty</h1>
    <h2 class="part-5">You have to believe <br />you’re on the <span class="yellow">right path<i class="line"></i></span></h2>
    <h2 class="part-6">You have to see a vision of the future</h2>
    <h2 class="part-7">And to feel like you’re <span class="yellow">getting there<i class="line"></i></span></h2>
  </div>
</div>
