<?php include('inc/header.php'); ?>

	<!-- Content -->

  <?php include('sections/_intro.php'); ?>

  <?php include('sections/_dwyl.php'); ?>

  <?php include('sections/_powerful.php'); ?>

  <?php include('sections/_problem.php'); ?>

  <?php include('sections/_needs.php'); ?>

  <?php include('sections/_i-am.php'); ?>

  <?php include('sections/_instagram.php'); ?>

  <?php include('sections/_place.php'); ?>

  <?php include('sections/_family.php'); ?>

  <?php include('sections/_world.php'); ?>

  <?php include('sections/_beauty.php'); ?>

  <?php include('sections/_you.php'); ?>

  <?php include('sections/_share.php'); ?>

  <?php include('sections/_environment.php'); ?>

  <?php include('sections/_countries.php'); ?>

  <?php include('sections/_growth.php'); ?>

  <?php include('sections/_people.php'); ?>

  <?php include('sections/_we.php'); ?>

  <?php include('sections/_progress.php'); ?>

  <?php include('sections/_reasons.php'); ?>

  <?php include('sections/_outro.php'); ?>

  <?php include('sections/_final.php'); ?>

	<!-- End Content -->

<?php include('inc/footer.php'); ?>
