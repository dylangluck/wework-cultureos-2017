


    <!-- Scripts -->
    <script type="text/javascript" src="/assets/js/libs/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/js/libs/modernizr.min.js"></script>
    <script type="text/javascript" src="/assets/js/libs/TweenMax.min.js"></script>
    <script type="text/javascript" src="/assets/js/libs/TimelineLite.min.js"></script>
    <script type="text/javascript" src="/assets/js/libs/ScrollMagic.min.js"></script>
    <script type="text/javascript" src="/assets/js/custom.min.js"></script>
  </body>
</html>
